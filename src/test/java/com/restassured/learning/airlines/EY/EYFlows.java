package com.restassured.learning.airlines.EY;

import com.restassured.learning.builders.BookingRequestBuilder;
import com.restassured.learning.common.abstraction.AirlineTestConfiguration;
import com.restassured.learning.common.utils.BookingFlowUtils;
import com.restassured.learning.common.utils.RequestSpecificationsUtils;
import com.restassured.learning.common.utils.ResponseSpecificationsUtils;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.openfreight.api.transformation.data.v1.booking.request.BookingRequest;
import org.openfreight.api.transformation.utils.XmlJsonMarshaller;

import java.io.IOException;

public class EYFlows {

    public Response getRatesFlow(AirlineTestConfiguration airlineTestConfiguration) throws IOException {

        RequestSpecification requestSpecification = RequestSpecificationsUtils.buildAvailabilityRequestSpecifications(airlineTestConfiguration);
        ResponseSpecification responseSpecification = ResponseSpecificationsUtils.buildResponseSpecifications(airlineTestConfiguration);

        Response response = RestAssured.given()
                .spec(requestSpecification)
                .when()
                .post(airlineTestConfiguration.getEndpointPath());

        response.then().spec(responseSpecification);

        return response;

    }

    public Response createBookingFlow(AirlineTestConfiguration airlineTestConfiguration) {

        RequestSpecification requestSpecification = RequestSpecificationsUtils.buildBookingRequestSpecifications(airlineTestConfiguration);
        ResponseSpecification responseSpecification = ResponseSpecificationsUtils.buildResponseSpecifications(airlineTestConfiguration);

        String createBookingRequestString = BookingFlowUtils.buildCreateBookingRequest(airlineTestConfiguration.getBookingRequestBuilder());

        BookingRequest bookingRequest = XmlJsonMarshaller.unmarshal(createBookingRequestString, BookingRequest.class);
        airlineTestConfiguration.setBookingRequest(bookingRequest);
        airlineTestConfiguration.getBookingRequestBuilder().setCreateBookingRequest(bookingRequest);

        Response response = RestAssured
                .given()
                .spec(requestSpecification)
                .body(createBookingRequestString)
                .when()
                .post(airlineTestConfiguration.getEndpointPath());

        response.then().spec(responseSpecification);

        return response;

    }

    public Response cancelBookingFlow(AirlineTestConfiguration airlineTestConfiguration) {

        RequestSpecification requestSpecification = RequestSpecificationsUtils.buildBookingRequestSpecifications(airlineTestConfiguration);
        ResponseSpecification responseSpecification = ResponseSpecificationsUtils.buildResponseSpecifications(airlineTestConfiguration);

        String cancelBookingRequest = BookingFlowUtils.buildCreateBookingRequest(airlineTestConfiguration.getBookingRequestBuilder());

        airlineTestConfiguration.setBookingRequest(XmlJsonMarshaller.unmarshal(cancelBookingRequest, BookingRequest.class));

        Response response =  RestAssured
                .given()
                .spec(requestSpecification)
                .body(cancelBookingRequest)
                .when()
                .post(airlineTestConfiguration.getEndpointPath());

        response.then().spec(responseSpecification);

        return response;
    }
}
