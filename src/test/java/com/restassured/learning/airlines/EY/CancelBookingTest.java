package com.restassured.learning.airlines.EY;

import com.restassured.learning.builders.BookingRequestBuilder;
import com.restassured.learning.common.abstraction.AbstractAirlineTest;
import com.restassured.learning.common.abstraction.AirlineTestConfiguration;
import com.restassured.learning.common.constants.CommonConstants;
import com.restassured.learning.common.portal.BQDataVerifier;
import com.restassured.learning.common.utils.BookingFlowUtils;
import io.restassured.response.Response;
import org.openfreight.api.transformation.data.v1.booking.response.BookingResponse;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CancelBookingTest extends AbstractAirlineTest {

    private BQDataVerifier bqDataVerifier = new BQDataVerifier();
    private EYFlows eyFlows = new EYFlows();

    @BeforeClass
    public void initBookingBuilder(ITestContext context) throws Exception {

        AirlineTestConfiguration createBookingConfiguration = (AirlineTestConfiguration) context.getAttribute(CommonConstants.CREATE_Booking_TEST_DATA);

        if (createBookingConfiguration == null) {
            throw new Exception("Failed to get createBookingConfiguration");
        }

        airlineTestConfiguration.setBookingRequestBuilder(createBookingConfiguration.getBookingRequestBuilder());
    }

    public CancelBookingTest() {
        super("profiles/EY/EtihadCancelBookingTest.xml");
    }

    @Test(testName = "Successfully Cancel Etihad Booking", groups = {"cancelBooking"}, dependsOnGroups = {"createBooking"})
    public void cancelBookingTest() throws Exception {

        Response restAssuredResponse = eyFlows.cancelBookingFlow(airlineTestConfiguration);

        BookingResponse bookingResponse = BookingFlowUtils.parseBookingResponse(restAssuredResponse);

        Assert.assertNotNull(bookingResponse);
        Assert.assertEquals("Acknowledgement", bookingResponse.getResponseStatus().getStatus().get(0).getCode().getValue());
        Assert.assertEquals("SUCCESS", bookingResponse.getResponseStatus().getStatus().get(0).getReason().getValue());

        // TODO Samer, Assert that the booking is canceled successfully, code == XX

        airlineTestConfiguration.setBookingResponse(bookingResponse);

        bqDataVerifier.verifyBookingRequest(airlineTestConfiguration);
    }

}
