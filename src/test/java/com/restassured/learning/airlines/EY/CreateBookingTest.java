package com.restassured.learning.airlines.EY;

import com.restassured.learning.builders.BookingRequestBuilder;
import com.restassured.learning.common.abstraction.AbstractAirlineTest;
import com.restassured.learning.common.abstraction.AirlineTestConfiguration;
import com.restassured.learning.common.constants.CodeAndMessage;
import com.restassured.learning.common.constants.CommonConstants;
import com.restassured.learning.common.portal.BQDataVerifier;
import com.restassured.learning.common.utils.BookingFlowUtils;
import io.restassured.response.Response;
import org.openfreight.api.transformation.data.v1.booking.request.BookingRequest;
import org.openfreight.api.transformation.data.v1.booking.response.BookingResponse;
import org.openfreight.api.transformation.utils.XmlJsonMarshaller;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class CreateBookingTest extends AbstractAirlineTest {

    private BQDataVerifier bqDataVerifier = new BQDataVerifier();
    private EYFlows eyFlows = new EYFlows();

    public CreateBookingTest() {
        super("profiles/EY/EtihadCreateBookingTest.xml");
    }

    @BeforeClass
    public void initBookingBuilder(ITestContext context) throws Exception {

        AirlineTestConfiguration ratesTestConfiguration = (AirlineTestConfiguration) context.getAttribute(CommonConstants.AVAILABILITY_TEST_DATA);

        if (ratesTestConfiguration == null) {
            throw new Exception("Failed to get Rate Flow Configuration");
        }

        airlineTestConfiguration.setBookingRequestBuilder(new BookingRequestBuilder(ratesTestConfiguration.getRateRequest(), ratesTestConfiguration.getRateResponse(), airlineTestConfiguration.getAWB()));

    }

    @Test(testName = "Successfully Place Etihad Booking", groups = {"createBooking"}, dependsOnGroups = {"availability"})
    public void bookingTest(ITestContext context) throws Exception {

        Response restAssuredResponse = eyFlows.createBookingFlow(airlineTestConfiguration);

        BookingResponse createBookingResponse = BookingFlowUtils.parseBookingResponse(restAssuredResponse);

        Assert.assertNotNull(createBookingResponse);
        Assert.assertEquals("Acknowledgement", createBookingResponse.getResponseStatus().getStatus().get(0).getCode().getValue());
        Assert.assertEquals("SUCCESS", createBookingResponse.getResponseStatus().getStatus().get(0).getReason().getValue());

        airlineTestConfiguration.setBookingResponse(createBookingResponse);
        airlineTestConfiguration.getBookingRequestBuilder().setCreateBookingResponse(createBookingResponse);

        bqDataVerifier.verifyBookingRequest(airlineTestConfiguration);

        // Move Booking Request/Response to be used in Booking Tests
        context.setAttribute(CommonConstants.CREATE_Booking_TEST_DATA, airlineTestConfiguration);
    }

}
