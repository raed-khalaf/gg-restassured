package com.restassured.learning.airlines.EY;

import com.restassured.learning.common.abstraction.AbstractAirlineTest;
import com.restassured.learning.common.constants.CommonConstants;
import com.restassured.learning.common.constants.RoutesEventsColumns;
import com.restassured.learning.common.portal.BQDataVerifier;
import com.restassured.learning.common.utils.AvailabilityFlowUtils;
import io.restassured.response.Response;
import org.junit.Assert;
import org.openfreight.api.transformation.data.v1.rates.response.RateResponse;
import org.openfreight.api.transformation.utils.XmlJsonMarshaller;
import org.testng.ITestContext;
import org.testng.annotations.Test;


public class AvailabilityTest extends AbstractAirlineTest {

    private BQDataVerifier bqDataVerifier = new BQDataVerifier();
    private EYFlows eyFlows = new EYFlows();

    public AvailabilityTest() {
        super("profiles/EY/EtihadAvailabilityTest.xml");
    }

    @Test(testName = "Successfully Get EY Rates", groups = {"availability"})
    public void getRatesTest(ITestContext context) throws Exception {

        Response response = eyFlows.getRatesFlow(airlineTestConfiguration);

        RateResponse rateResponse = AvailabilityFlowUtils.parseResponse(response);

        Assert.assertNotNull(rateResponse);

        int totalResults = 0;

        if (rateResponse.getRates() != null && rateResponse.getRates().getRate() != null) {
            totalResults = rateResponse.getRates().getRate().size();
        }

        if (totalResults == 0) { // No Rates Found
            throw new Exception("Failed to fetch RateResponse with Results");
        }

        // Reflect Rate Flow to Airline Configuration container
        airlineTestConfiguration.setRateResponse(rateResponse);
        airlineTestConfiguration.getExpectedBQProperties().put(RoutesEventsColumns.TOTAL_RESULTS, totalResults + "");

        // Verify BQ Data
        bqDataVerifier.verifyAvailabilityRequest(airlineTestConfiguration);

        // Move Rates Request/Response to be used in Booking Tests
        context.setAttribute(CommonConstants.AVAILABILITY_TEST_DATA, airlineTestConfiguration);
    }

}
