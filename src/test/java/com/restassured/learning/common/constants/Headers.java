package com.restassured.learning.common.constants;

public class Headers {

    /** HEADER KEY **/
    public static final String USER_EMAIL = "user-email";
    public static final String CONTENT_TYPE = "content-type";
    public static final String APP_ID = "appId";

    /** HEADER VALUE **/
    public static final String APPLICATION_XML = "application/xml";
    public static final String APPLICATION_JSON = "application/json";
}
