package com.restassured.learning.common.constants;

/**
 * @Author Raed Khalaf
 * @Since 9/7/2020
 * <p>
 * Contains constants used for monitorings
 */
public class RoutesEventsColumns {

    public static final String SERVICE_NAME = "serviceName";
    public static final String ROUTE_ID = "routeID";
    public static final String EXCHANGE_ID = "exchangeID";
    public static final String MESSAGE_ID = "messageID";
    public static final String PROVIDER_ID = "providerID";
    public static final String PROVIDER_COMMUNICATION = "providerCommunication";
    public static final String CONSUMER_ID = "consumerID";
    public static final String CONSUMER_COMMUNICATION = "consumerCommunication";
    public static final String IN_FILE_NAME = "inFileName";
    public static final String OUT_FILE_NAME = "outfileName";
    public static final String IN_MESSAGE_SIZE = "inMessageSize";
    public static final String OUT_MESSAGE_SIZE = "outMessageSize";
    public static final String IN_MESSAGE = "inMessage";
    public static final String OUT_MESSAGE = "outMessage";
    public static final String START_TIME_STAMP = "startTimeStamp";
    public static final String FINISH_TIME_STAMP = "finishTimeStamp";
    public static final String ELAPSED_TIME = "elapsedTime";
    public static final String MESSAGE_HISTORY = "messageHistory";
    public static final String STATUS = "status";
    public static final String REFERENCE_ID = "referenceID";
    public static final String CREATE_DATE = "createDate";
    public static final String MAIN_KEY = "mainKey";
    public static final String APP_ID = "appId";
    public static final String HOST = "host";
    public static final String REMOTE_HOST = "remoteHost";
    public static final String REMOTE_ADDRESS = "remoteAddress";
    public static final String ENVIRONMENT = "environment";
    public static final String TIMEOUT_MS = "timeoutMS";
    public static final String THIRD_PARTY_REQUEST = "thirdpartyRequest";
    public static final String THIRD_PARTY_RESPONSE = "thirdpartyResponse";
    public static final String REQUEST_ID = "requestID";
    public static final String USER_EMAIL = "userEmail";
    public static final String NUMBER_OF_RESULTS = "numberOfResults";
    public static final String PATH = "path";
    public static final String MODE = "mode";
    public static final String THIRD_PARTY_REQUEST_URL = "thirdpartyRequestUrl";
    public static final String TOTAL_RESULTS = "totalResults";
    public static final String JMS_MESSAGE_ID = "JMSMessageID";
    public static final String JMSX_DELIVERY_COUNT = "JMSXDeliveryCount";
    public static final String JMSX_GROUP_ID = "JMSXGroupID";
    public static final String THIRD_PARTY_REQUEST_HEADERS = "thirdPartyRequestHeaders";
    public static final String RESPONSE_HEADERS = "responseHeaders";
    public static final String REQUEST_HEADERS = "requestHeaders";
    public static final String CUSTOM_HEADERS = "customHeaders";
    public static final String SERVICE_METHOD = "serviceMethod";
    public static final String CARRIER = "carrier";
    public static final String ORIGIN = "origin";
    public static final String ORIGIN_COUNTRY = "originCountry";
    public static final String DESTINATION = "destination";
    public static final String DESTINATION_COUNTRY = "destinationCountry";
    public static final String TRANSPORT_TYPE = "transportType";
    public static final String AWB = "AWB";

}
