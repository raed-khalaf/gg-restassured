package com.restassured.learning.common.constants;

public class CommonConstants {

    // Contexts Availability Configuration Key
    public static final String AVAILABILITY_TEST_DATA = "AvailabilityTestData";
    public static final String CREATE_Booking_TEST_DATA = "BookingTestData";

}
