package com.restassured.learning.common.constants;


import java.util.HashMap;
import java.util.Map;

public enum CodeAndMessage {

    CHECKSUM_MISMATCH(2, "Checksum mismatched between inbound message and downloaded message"), //
    FAILED_TO_WRITE_ON_BIG_QUERY(5, "Exception while writing on big query"), //
    RESTLET_COMMUNICATION_EXCEPTION(6, "Failed to connect to restlet endpoint"), //
    FAILED_TO_GET_LAST_ADDED_ROUTE_EVENT(9, "Error getting last added route event"),
    PROFILE_INITIALIZATION_ERROR(10, "Failed to initialize service"),

    INVALID_CREDENTIALS(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Inavlid Credentials"),
    INVALID_TOKEN(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Invalid Token, please request a new one"),
    IVALID_RESPONSE_FROM_FREIGHTOS(HttpStatusCodes.STATUS_CODE_SERVER_ERROR, "Invalid response format"),
    MISSING_OR_INVALID_AUTH_HEADER(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Missing or invalid Authorization Header"),
    SERVER_ERROR(HttpStatusCodes.STATUS_CODE_SERVER_ERROR, "Unknown Server error"),
    MISSING_API_CONFIGURATIONS(10, "Missing API configurations"),

    // FTP common errors
    FTP_CONNECTION_CLOSED(426, "Connection closed; transfer aborted"),
    FTP_CANNOT_OPEN_DATA_CONNECTION(425, "Can't open data connection"),
    FTP_UNAUTHORIZED(530, "Not logged in"),
    FTP_INVALID_FILE_NAME(553, "Requested action not taken. File name not allowed"),
    FTP_CONNECTION_CLOSED_BY_HOST(10054, "Connection Reset by Peer - The connection was forcibly closed by the remote host"),
    FTP_TIME_OUT(10060, "Cannot connect to remote server, Generally a time-out error"),
    FTP_SERVER_IS_FULL(10068, "Too many users, server is full. Contact the server administrator"),

    BAD_REQUEST(440, "Invalid incoming message"),
    NOT_FOUND(404, "Not found"),
    BQ_EXCEPTION(540, "BQ Exception"),
    NOT_SUPPORTED_OD(440, "Not supported Origin or Destination type"),
    NOT_ALLOWED_OD(440, "Not Allowed Origin or Destination"),
    NOT_SUPPORTED_OD_VALUE(440, "Not supported Origin or Destination value"),
    NOT_SUPPORTED_LOAD_TYPE(440, "Not supported load type"),
    NOT_SUPPORTED_PCTL(440, "Unsupported percentile value"),
    NOT_SUPPORTED_MULTIPLE_LOAD_TYPES(440, "Not supported multiple load types"),
    NOT_SUPPORTED_LOAD_MEASUREMENTS(440, "Not supported load measurements"),
    NOT_SUPPORTED_BULK_REQUESTS(440, "Not supported bulk requests"),
    NOT_SUPPORTED_ENUM(440, "Not supported enum value"),
    NOT_SUPPORTED_BASE_UNIT(440, "Not supported base unit"),
    NOT_SUPPORTED_CURRENCY_CODE(440, "Not supported currency code"),
    NOT_SUPPORTED_DATE_FORMAT(440, "Not supported date format"),
    TIME_OUT(504, "Gateway Timeout"),
    FNA_MESSAGE_RECEIVED(200, "FNA Received"),
    UNEXPECTED_ERROR(540, "Unexpected Error"),
    NO_MESSAGES_FOUND(200, "No Message Found"),
    NOT_SUPPORTED_OD_MODE(440, "Not supported Origin or Destination in specific mode"),
    INVALID_REF_ID(440, "Missing or invalid referenceId header parameter"),
    INVALID_MSG_ID(440, "Missing or invalid messageId header parameter"),
    INVALID_API_RESPONSE(540, "Invalid API Response"),
    EMPTY_API_RESPONSE(540, "Empty API Response"),
    NOT_SUPPORTED_CARGO_TYPE(440, "Not supported cargo type"),
    MISSING_ORIGIN_MAPPING(440, "Missing Mapping for Origin"),
    MISSING_DESTINATION_MAPPING(440, "Missing Mapping for Destination"),
    MISSING_DATABASE_MAPPING(440, "Missing Mapping for input"),
    MISSING_AUTH_USERNAME_MAPPING(440, "Missing username credential mapping"),
    MISSING_AUTH_PASSWORD_MAPPING(440, "Missing password credential mapping"),
    MISSING_AUTH_NONCE_MAPPING(440, "Missing nonce credential mapping"),
    MISSING_AUTH_USERNAME_TOKEN_ID_MAPPING(440, "Missing usernameTokenId credential mapping"),
    RESPONSE_BODY_CONTAINS_EMPTY_FIELDS(540, "Response body contains empty fields"),
    NOT_SUPPORTED_MULTIPLE_ORIGIN_OR_MULTIPLE_DESTINATION_TYPES(440, "Not supported multiple origin or multiple destination types"),
    NOT_SUPPORTED_MODE(440, "Not supported mode"),
    OVERWEIGHT(440, "Overweight"),
    OVERVOLUME(440, "Overvolume"),
    CORS_ORIGIN_NOT_ALLOWED(440, "Request Origin does not match host rules"),
    SSO_FAILED(200, "SSO Failed"),
    NOT_ALLOWED_COMMODITY(440, "Not Allowed Commodity Type"),
    MISSING_OR_INVALID_HEADER(440, "Missing or invalid Header"),
    MISSING_OR_INVALID_PARAMETER(440, "Missing or invalid Query Parameter"),
    MISSING_OR_INVALID_PATH_PARAMETER(440, "Missing or Invalid Path Parameter"),

    /*Plain Http codes*/
    OK(HttpStatusCodes.STATUS_CODE_OK, "OK"),
    UNAUTHORIZED(HttpStatusCodes.STATUS_CODE_UNAUTHORIZED, "Unauthorized"),
    PARTIAL_CONTENT(206, "Partial Content"),
    INTERNAL_SERVER_ERROR(500, "Internal Server Error"),
    CREATED(201, "CREATED"),
    HTTP_BAD_REQUEST(400, "Bad request"),
    TOO_MANY_REQUESTS(429, "Too Many Requests"),

    PICKUP_DATE_EXCEED_90_DAYS(440, "Pickup date cannot be more than 90 days in the future"),
    PICKUP_DATE_IN_PAST(440, "Pickup date cannot be in the past"),
    NOT_SUPPORTED_ACCESSORIAL(440, "Accessorial is not supported"),
    NO_MOCK_RESPONSE(540, "No mock response found");

    public final String message;
    public final int code;

    public final static Map<Integer, CodeAndMessage> HTTP_CODE_AND_MESSAGE;
    public static CodeAndMessage[] httpCodes = {OK, UNAUTHORIZED, PARTIAL_CONTENT, INTERNAL_SERVER_ERROR, CREATED, NOT_FOUND, HTTP_BAD_REQUEST, TOO_MANY_REQUESTS};

    static {
        Map<Integer, CodeAndMessage> map = new HashMap<Integer, CodeAndMessage>();
        for (CodeAndMessage codeAndMessage : httpCodes) {
            map.put(codeAndMessage.code, codeAndMessage);
        }
        HTTP_CODE_AND_MESSAGE = map;
    }

    /**
     * This function returns a plain HTTP code and message enum based on the code
     *
     * @return
     */
    public static CodeAndMessage getPlainHttpCodeEnum(int code) {
        return HTTP_CODE_AND_MESSAGE.get(code);
    }

    CodeAndMessage(int code, String message) {
        this.message = message;
        this.code = code;
    }

}

class HttpStatusCodes {
    public static final int STATUS_CODE_OK = 200;
    public static final int STATUS_CODE_NO_CONTENT = 204;
    public static final int STATUS_CODE_MULTIPLE_CHOICES = 300;
    public static final int STATUS_CODE_MOVED_PERMANENTLY = 301;
    public static final int STATUS_CODE_FOUND = 302;
    public static final int STATUS_CODE_SEE_OTHER = 303;
    public static final int STATUS_CODE_NOT_MODIFIED = 304;
    public static final int STATUS_CODE_TEMPORARY_REDIRECT = 307;
    public static final int STATUS_CODE_UNAUTHORIZED = 401;
    public static final int STATUS_CODE_FORBIDDEN = 403;
    public static final int STATUS_CODE_NOT_FOUND = 404;
    public static final int STATUS_CODE_CONFLICT = 409;
    public static final int STATUS_CODE_SERVER_ERROR = 500;
    public static final int STATUS_CODE_BAD_GATEWAY = 502;
    public static final int STATUS_CODE_SERVICE_UNAVAILABLE = 503;

    public HttpStatusCodes() {
    }

    public static boolean isSuccess(int statusCode) {
        return statusCode >= 200 && statusCode < 300;
    }

    public static boolean isRedirect(int statusCode) {
        switch (statusCode) {
            case 301:
            case 302:
            case 303:
            case 307:
                return true;
            case 304:
            case 305:
            case 306:
            default:
                return false;
        }
    }
}
