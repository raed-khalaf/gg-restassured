package com.restassured.learning.common.utils;

import com.restassured.learning.common.abstraction.AirlineTestConfiguration;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.ResponseSpecification;

public class ResponseSpecificationsUtils {

    /**
     * @return  ResponseSpecifications which contains
     * 1. expected status code
     * 2. expected content type
     * 3. ...etc
     */
    public static ResponseSpecification buildResponseSpecifications(AirlineTestConfiguration airlineTestConfiguration) {

        ResponseSpecBuilder responseSpecBuilder = new ResponseSpecBuilder();
        responseSpecBuilder.expectContentType(airlineTestConfiguration.getExpectedContentType());
        responseSpecBuilder.expectStatusCode(Integer.parseInt(airlineTestConfiguration.getExpectedStatusCode()));

        return responseSpecBuilder.build();

    }

}
