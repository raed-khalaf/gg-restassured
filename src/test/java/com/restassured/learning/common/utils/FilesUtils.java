package com.restassured.learning.common.utils;

import java.io.File;
import java.io.IOException;

public class FilesUtils {

    public static String readFile(String resourcePath) throws IOException {
        ClassLoader classLoader = FilesUtils.class.getClassLoader();
        File availabilityRequestFile = new File(classLoader.getResource(resourcePath).getFile());
        return org.apache.commons.io.FileUtils.readFileToString(availabilityRequestFile);
    }

}
