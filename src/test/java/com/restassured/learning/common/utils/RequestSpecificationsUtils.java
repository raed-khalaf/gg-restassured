package com.restassured.learning.common.utils;

import com.restassured.learning.common.abstraction.AirlineTestConfiguration;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.specification.RequestSpecification;
import org.openfreight.api.transformation.data.v1.common.LocationType;
import org.openfreight.api.transformation.data.v1.rates.request.RateRequest;
import org.openfreight.api.transformation.utils.XmlJsonMarshaller;

import java.io.IOException;
import java.util.Map;

public class RequestSpecificationsUtils {

    public static RequestSpecification buildAvailabilityRequestSpecifications(AirlineTestConfiguration airlineTestConfiguration) throws IOException {

        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

        String rateRequestString = getRateRequestFromResource(airlineTestConfiguration.getRequestFilePath(), airlineTestConfiguration);
        requestSpecBuilder.setBody(rateRequestString);

        injectRequestHeaders(requestSpecBuilder, airlineTestConfiguration);

        return requestSpecBuilder.build();

    }

    public static RequestSpecification buildAvailabilityRequestSpecifications(AirlineTestConfiguration airlineTestConfiguration, String resourcePath) throws IOException {

        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

        String rateRequestString = getRateRequestFromResource(resourcePath, airlineTestConfiguration);
        requestSpecBuilder.setBody(rateRequestString);

        injectRequestHeaders(requestSpecBuilder, airlineTestConfiguration);

        return requestSpecBuilder.build();

    }

    public static String getRateRequestFromResource(String resourcePath, AirlineTestConfiguration airlineTestConfiguration) throws IOException {
        String availabilityRequestBody = FilesUtils.readFile(resourcePath);
        RateRequest rateRequest = XmlJsonMarshaller.unmarshal(availabilityRequestBody, RateRequest.class);
        // TODO set dates TOA, LAD dynamically
        airlineTestConfiguration.setRateRequest(rateRequest);
        return XmlJsonMarshaller.marshal(rateRequest, RateRequest.class);
    }

    public static void injectRequestHeaders(RequestSpecBuilder requestSpecBuilder, AirlineTestConfiguration airlineTestConfiguration) {
        if (airlineTestConfiguration.getRequestHeaders() != null) {
            // inject Request headers
            for (Map.Entry<String, String> header : airlineTestConfiguration.getRequestHeaders().entrySet()) {
                requestSpecBuilder.addHeader(header.getKey(), header.getValue());
            }
        }
    }

    public static RequestSpecification buildBookingRequestSpecifications(AirlineTestConfiguration airlineTestConfiguration) {

        RequestSpecBuilder requestSpecBuilder = new RequestSpecBuilder();

        injectRequestHeaders(requestSpecBuilder, airlineTestConfiguration);

        return requestSpecBuilder.build();

    }
}
