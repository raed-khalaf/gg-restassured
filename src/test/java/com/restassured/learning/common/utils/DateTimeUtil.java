package com.restassured.learning.common.utils;

import java.util.Calendar;
import java.util.Date;

public class DateTimeUtil {

  /**
   * returns a modified version of "date" by adding the specified number of
   * seconds to it<br>
   * You can subtract seconds by providing a negative number
   */
  public static Date addSeconds(Date date, int seconds) {
    Calendar cal = Calendar.getInstance();
    cal.setTime(date);
    cal.add(Calendar.SECOND, seconds);
    return cal.getTime();
  }

}
