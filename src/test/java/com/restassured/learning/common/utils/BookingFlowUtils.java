package com.restassured.learning.common.utils;

import com.restassured.learning.builders.BookingRequestBuilder;
import com.restassured.learning.common.abstraction.AirlineTestConfiguration;
import io.restassured.response.Response;
import org.openfreight.api.transformation.data.v1.booking.request.BookingRequest;
import org.openfreight.api.transformation.data.v1.booking.response.BookingResponse;
import org.openfreight.api.transformation.utils.XmlJsonMarshaller;

public class BookingFlowUtils {

    public static String buildCreateBookingRequest(BookingRequestBuilder bookingRequestBuilder) {

        BookingRequest createBookingReq = bookingRequestBuilder.build();

        return XmlJsonMarshaller.marshal(createBookingReq, BookingRequest.class);

    }

    public static BookingResponse parseBookingResponse(Response createBookingRestAssuredResponse) {

        String createBookingResStr = createBookingRestAssuredResponse.getBody().print();

        return XmlJsonMarshaller.unmarshal(createBookingResStr, BookingResponse.class);

    }

}
