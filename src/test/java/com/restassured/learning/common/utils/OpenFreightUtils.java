package com.restassured.learning.common.utils;

import org.openfreight.api.transformation.data.v1.common.DocumentIdentifierType;
import org.openfreight.api.transformation.data.v1.common.DocumentIdentifiers;
import org.openfreight.api.transformation.data.v1.common.DocumentType;
import org.openfreight.api.transformation.data.v1.common.TextType;

import java.util.ArrayList;

public class OpenFreightUtils {

    /**
     * Build DocumentIdentifiers list with one DocumentIdentifier of type @param documentType
     *
     * @param documentID
     * @param documentType
     * @return
     */
    public static DocumentIdentifiers buildDocumentIdentifiers(String documentID,
                                                               DocumentType documentType) {
        DocumentIdentifiers documentIdentifiers = new DocumentIdentifiers();
        documentIdentifiers.setDocumentIdentifier(new ArrayList<DocumentIdentifierType>());
        documentIdentifiers.getDocumentIdentifier().add(buildDocumentIdentifier(documentID,documentType));
        return documentIdentifiers;
    }


    /**
     * Build Single DocumentIdentifierType with documentID and document type
     *
     * @param documentID
     * @param documentType
     * @return
     */
    public static DocumentIdentifierType buildDocumentIdentifier(String documentID, DocumentType documentType) {

        DocumentIdentifierType documentIdentifier = new DocumentIdentifierType();
        documentIdentifier.setDocumentType(documentType);
        documentIdentifier.setDocumentID(new TextType(documentID));
        return documentIdentifier;
    }
}
