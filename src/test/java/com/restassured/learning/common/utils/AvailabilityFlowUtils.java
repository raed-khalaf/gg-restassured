package com.restassured.learning.common.utils;

import io.restassured.response.Response;
import org.openfreight.api.transformation.data.v1.rates.response.RateResponse;
import org.openfreight.api.transformation.utils.XmlJsonMarshaller;

public class AvailabilityFlowUtils {

    public static RateResponse parseResponse(Response response) {
        String availabilityResponseString = response.getBody().print();
        return XmlJsonMarshaller.unmarshal(availabilityResponseString, RateResponse.class);
    }

}
