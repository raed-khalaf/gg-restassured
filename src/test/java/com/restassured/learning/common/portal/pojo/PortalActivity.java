package com.restassured.learning.common.portal.pojo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "requestId",
        "referenceId",
        "createDate",
        "routeId",
        "status",
        "remoteHost",
        "userEmail",
        "mode",
        "totalResults",
        "path",
        "customHeaders",
        "appId",
        "remoteAddress",
        "elapsedTime",
        "startTimeStamp",
        "inMessage",
        "outMessage",
        "thirdPartyRequestUrl",
        "thirdPartyRequest",
        "thirdPartyResponse",
        "requestHeaders",
        "responseHeaders",
        "thirdPartyRequestHeaders"
})
public class PortalActivity implements Serializable {

    @JsonProperty("requestId")
    private String requestId;
    @JsonProperty("referenceId")
    private String referenceId;
    @JsonProperty("createDate")
    private String createDate;
    @JsonProperty("routeId")
    private String routeId;
    @JsonProperty("status")
    private String status;
    @JsonProperty("remoteHost")
    private String remoteHost;
    @JsonProperty("userEmail")
    private String userEmail;
    @JsonProperty("mode")
    private String mode;
    @JsonProperty("totalResults")
    private String totalResults;
    @JsonProperty("path")
    private String path;
    @JsonProperty("customHeaders")
    private String customHeaders;
    @JsonProperty("appId")
    private String appId;
    @JsonProperty("remoteAddress")
    private String remoteAddress;
    @JsonProperty("elapsedTime")
    private String elapsedTime;
    @JsonProperty("startTimeStamp")
    private String startTimeStamp;
    @JsonProperty("inMessage")
    private String inMessage;
    @JsonProperty("outMessage")
    private String outMessage;
    @JsonProperty("thirdPartyRequestUrl")
    private String thirdPartyRequestUrl;
    @JsonProperty("thirdPartyRequest")
    private String thirdPartyRequest;
    @JsonProperty("thirdPartyResponse")
    private String thirdPartyResponse;
    @JsonProperty("requestHeaders")
    private String requestHeaders;
    @JsonProperty("responseHeaders")
    private String responseHeaders;
    @JsonProperty("thirdPartyRequestHeaders")
    private String thirdPartyRequestHeaders;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("requestId")
    public String getRequestId() {
        return requestId;
    }

    @JsonProperty("requestId")
    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    @JsonProperty("referenceId")
    public String getReferenceId() {
        return referenceId;
    }

    @JsonProperty("referenceId")
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    @JsonProperty("createDate")
    public String getCreateDate() {
        return createDate;
    }

    @JsonProperty("createDate")
    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    @JsonProperty("routeId")
    public String getRouteId() {
        return routeId;
    }

    @JsonProperty("routeId")
    public void setRouteId(String routeId) {
        this.routeId = routeId;
    }

    @JsonProperty("status")
    public String getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(String status) {
        this.status = status;
    }

    @JsonProperty("remoteHost")
    public String getRemoteHost() {
        return remoteHost;
    }

    @JsonProperty("remoteHost")
    public void setRemoteHost(String remoteHost) {
        this.remoteHost = remoteHost;
    }

    @JsonProperty("userEmail")
    public String getUserEmail() {
        return userEmail;
    }

    @JsonProperty("userEmail")
    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @JsonProperty("mode")
    public String getMode() {
        return mode;
    }

    @JsonProperty("mode")
    public void setMode(String mode) {
        this.mode = mode;
    }

    @JsonProperty("totalResults")
    public String getTotalResults() {
        return totalResults;
    }

    @JsonProperty("totalResults")
    public void setTotalResults(String totalResults) {
        this.totalResults = totalResults;
    }

    @JsonProperty("path")
    public String getPath() {
        return path;
    }

    @JsonProperty("path")
    public void setPath(String path) {
        this.path = path;
    }

    @JsonProperty("customHeaders")
    public String getCustomHeaders() {
        return customHeaders;
    }

    @JsonProperty("customHeaders")
    public void setCustomHeaders(String customHeaders) {
        this.customHeaders = customHeaders;
    }

    @JsonProperty("appId")
    public String getAppId() {
        return appId;
    }

    @JsonProperty("appId")
    public void setAppId(String appId) {
        this.appId = appId;
    }

    @JsonProperty("remoteAddress")
    public String getRemoteAddress() {
        return remoteAddress;
    }

    @JsonProperty("remoteAddress")
    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    @JsonProperty("elapsedTime")
    public String getElapsedTime() {
        return elapsedTime;
    }

    @JsonProperty("elapsedTime")
    public void setElapsedTime(String elapsedTime) {
        this.elapsedTime = elapsedTime;
    }

    @JsonProperty("startTimeStamp")
    public String getStartTimeStamp() {
        return startTimeStamp;
    }

    @JsonProperty("startTimeStamp")
    public void setStartTimeStamp(String startTimeStamp) {
        this.startTimeStamp = startTimeStamp;
    }

    @JsonProperty("inMessage")
    public String getInMessage() {
        return inMessage;
    }

    @JsonProperty("inMessage")
    public void setInMessage(String inMessage) {
        this.inMessage = inMessage;
    }

    @JsonProperty("outMessage")
    public String getOutMessage() {
        return outMessage;
    }

    @JsonProperty("outMessage")
    public void setOutMessage(String outMessage) {
        this.outMessage = outMessage;
    }

    @JsonProperty("thirdPartyRequestUrl")
    public String getThirdPartyRequestUrl() {
        return thirdPartyRequestUrl;
    }

    @JsonProperty("thirdPartyRequestUrl")
    public void setThirdPartyRequestUrl(String thirdPartyRequestUrl) {
        this.thirdPartyRequestUrl = thirdPartyRequestUrl;
    }

    @JsonProperty("thirdPartyRequest")
    public String getThirdPartyRequest() {
        return thirdPartyRequest;
    }

    @JsonProperty("thirdPartyRequest")
    public void setThirdPartyRequest(String thirdPartyRequest) {
        this.thirdPartyRequest = thirdPartyRequest;
    }

    @JsonProperty("thirdPartyResponse")
    public String getThirdPartyResponse() {
        return thirdPartyResponse;
    }

    @JsonProperty("thirdPartyResponse")
    public void setThirdPartyResponse(String thirdPartyResponse) {
        this.thirdPartyResponse = thirdPartyResponse;
    }

    @JsonProperty("requestHeaders")
    public String getRequestHeaders() {
        return requestHeaders;
    }

    @JsonProperty("requestHeaders")
    public void setRequestHeaders(String requestHeaders) {
        this.requestHeaders = requestHeaders;
    }

    @JsonProperty("responseHeaders")
    public String getResponseHeaders() {
        return responseHeaders;
    }

    @JsonProperty("responseHeaders")
    public void setResponseHeaders(String responseHeaders) {
        this.responseHeaders = responseHeaders;
    }

    @JsonProperty("thirdPartyRequestHeaders")
    public String getThirdPartyRequestHeaders() {
        return thirdPartyRequestHeaders;
    }

    @JsonProperty("thirdPartyRequestHeaders")
    public void setThirdPartyRequestHeaders(String thirdPartyRequestHeaders) {
        this.thirdPartyRequestHeaders = thirdPartyRequestHeaders;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}