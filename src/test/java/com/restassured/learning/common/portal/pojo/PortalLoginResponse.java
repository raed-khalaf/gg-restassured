package com.restassured.learning.common.portal.pojo;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "detailedMessage",
    "status",
    "token"
})
public class PortalLoginResponse {

  @JsonProperty("detailedMessage")
  public DetailedMessage detailedMessage;
  @JsonProperty("status")
  public String status;
  @JsonProperty("token")
  public String token;

}


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "warnings",
    "errors",
    "info"
})
class DetailedMessage {

  @JsonProperty("warnings")
  public List<Object> warnings = null;
  @JsonProperty("errors")
  public List<String> errors = null;
  @JsonProperty("info")
  public List<Object> info = null;

}
