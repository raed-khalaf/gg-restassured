package com.restassured.learning.common.portal;

import com.restassured.learning.common.abstraction.AirlineTestConfiguration;
import com.restassured.learning.common.constants.RoutesEventsColumns;
import com.restassured.learning.common.portal.pojo.PortalLoginResponse;
import com.restassured.learning.common.utils.DateTimeUtil;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.openfreight.api.transformation.utils.XmlJsonMarshaller;
import org.openfreight.api.transformation.utils.XmlJsonMarshaller.MediaType;
import org.testng.Assert;

public class BQDataVerifier {

  public void verifyAvailabilityRequest(AirlineTestConfiguration airlineTestConfiguration) throws Exception {

    long dateBefore = DateTimeUtil.addSeconds(new Date(), -100).getTime();
    long dateAfter = DateTimeUtil.addSeconds(new Date(), 100).getTime();

    List<LinkedHashMap<String, String>> portalActivities = fetchPortalActivities(
        airlineTestConfiguration.getRateResponse().getMessageHeader().getMessageID().getValue(),
        airlineTestConfiguration.getRateResponse().getMessageHeader().getRequestID().getValue(), dateBefore, dateAfter);

    verifyPropertiesWithExpectations(airlineTestConfiguration, portalActivities);

  }

  public void verifyBookingRequest(AirlineTestConfiguration airlineTestConfiguration) throws Exception {

    long dateBefore = DateTimeUtil.addSeconds(new Date(), -100).getTime();
    long dateAfter = DateTimeUtil.addSeconds(new Date(), 100).getTime();

    List<LinkedHashMap<String, String>> portalActivities = fetchPortalActivities(
            airlineTestConfiguration.getBookingResponse().getMessageHeader().getMessageID().getValue(),
            airlineTestConfiguration.getBookingResponse().getMessageHeader().getRequestID().getValue(), dateBefore, dateAfter);

    verifyPropertiesWithExpectations(airlineTestConfiguration, portalActivities);

  }

  private void verifyPropertiesWithExpectations(AirlineTestConfiguration airlineTestConfiguration, List<LinkedHashMap<String, String>> portalActivities) throws Exception {

    LinkedHashMap<String, String> mainActivity = null;

    // Verify expected Params
    for (LinkedHashMap<String, String> portalActivity : portalActivities) {
      if (portalActivity.get(RoutesEventsColumns.PATH).equals(airlineTestConfiguration.getMethod()+":"+airlineTestConfiguration.getEndpointPath())) {
        mainActivity = portalActivity;
        break;
      }
    }

    if (mainActivity == null) {
      throw new Exception("Portal Activity not found");
    }

    for (Map.Entry<String, String> expectedData : airlineTestConfiguration.getExpectedBQProperties().entrySet()) {
      Assert.assertEquals(mainActivity.get(expectedData.getKey()), expectedData.getValue());
    }

    for (Map.Entry<String, String> validator : airlineTestConfiguration.getBqDataValidators().entrySet()) {

      switch (validator.getKey()) {
        case "activities-count":
          Assert.assertEquals(portalActivities.size(), Integer.parseInt(validator.getValue()));
          break;
      }

    }

  }

  private List<LinkedHashMap<String, String>> fetchPortalActivities(String referenceId, String requestId, long fromDate,
                                                                    long toDate) {

    Response loginResponse = RestAssured.given()
        .formParam("username", "fps@freightos.com")
        .formParam("password", "Fre123!@#")
        .when()
        .post("https://integration-pre.test-freightos.com/goldengate/rest/api/login");

    PortalLoginResponse portalLoginResponse = XmlJsonMarshaller
        .unmarshal(loginResponse.getBody().print(), PortalLoginResponse.class, MediaType.JSON);

    Response activityResponse = RestAssured.given()
        .header(new Header("authorization",
            "Bearer " + portalLoginResponse.token))
        .when()
        .get(String.format(
            "https://integration-pre.test-freightos.com/portal-apis/rest/v1/activity?requestId=%s&referenceId=%s&action=view&fromDate=%d&toDate=%d",
            requestId, referenceId, fromDate, toDate));

    String activityResStr = activityResponse.getBody().print();
    List<LinkedHashMap<String, String>> portalActivities = XmlJsonMarshaller
        .unmarshal(activityResStr, List.class, MediaType.JSON);

    return portalActivities;
  }

}
