package com.restassured.learning.common.abstraction;

import com.restassured.learning.common.utils.FilesUtils;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.openfreight.api.transformation.data.v1.rates.request.RateRequest;
import org.openfreight.api.transformation.utils.XmlJsonMarshaller;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.util.Map;

/**
 *
 * Abstract layer for airlines Test classes
 * holds the common preparations and common logic
 *
 */
abstract public class AbstractAirlineTest {

    /**
     * Holds Configuration for the Airline
     */
    protected AirlineTestConfiguration airlineTestConfiguration;

    /**
     * Config file name
     */
    protected String configProfile;

    public AbstractAirlineTest(String configProfile) {
        this.configProfile = configProfile;
    }

    @BeforeClass
    public void initTest() throws IOException {

        this.prepareConfiguration();

        RestAssured.baseURI = "https://integration-pre.test-freightos.com";

    }

    protected void prepareConfiguration() throws IOException {
        String configurations = FilesUtils.readFile(configProfile);
        this.airlineTestConfiguration = XmlJsonMarshaller.unmarshal(configurations, AirlineTestConfiguration.class);
    }

}
