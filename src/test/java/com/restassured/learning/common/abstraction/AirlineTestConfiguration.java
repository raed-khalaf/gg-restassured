package com.restassured.learning.common.abstraction;

import com.restassured.learning.builders.BookingRequestBuilder;
import org.openfreight.api.transformation.data.v1.booking.request.BookingRequest;
import org.openfreight.api.transformation.data.v1.booking.response.BookingResponse;
import org.openfreight.api.transformation.data.v1.rates.request.RateRequest;
import org.openfreight.api.transformation.data.v1.rates.response.RateResponse;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.HashMap;

@XmlRootElement(name = "airlineConfiguration")
public class AirlineTestConfiguration implements Serializable {

    private RateRequest rateRequest;
    private RateResponse rateResponse;
    private BookingRequest bookingRequest;
    private BookingResponse bookingResponse;
    private BookingRequestBuilder bookingRequestBuilder;
    private String AWB;
    private String requestFilePath;
    private String endpointPath;
    private String method;
    private HashMap<String, String> requestHeaders;
    private String expectedContentType;
    private String expectedStatusCode;
    private HashMap<String, String> expectedBQProperties;
    private HashMap<String, String> bqDataValidators;

    public BookingRequest getBookingRequest() {
        return bookingRequest;
    }

    public void setBookingRequest(BookingRequest bookingRequest) {
        this.bookingRequest = bookingRequest;
    }

    public BookingResponse getBookingResponse() {
        return bookingResponse;
    }

    public void setBookingResponse(BookingResponse bookingResponse) {
        this.bookingResponse = bookingResponse;
    }

    public RateRequest getRateRequest() {
        return rateRequest;
    }

    public void setRateRequest(RateRequest rateRequest) {
        this.rateRequest = rateRequest;
    }

    public RateResponse getRateResponse() {
        return rateResponse;
    }

    public void setRateResponse(RateResponse rateResponse) {
        this.rateResponse = rateResponse;
    }

    public String getAWB() {
        return AWB;
    }

    public void setAWB(String AWB) {
        this.AWB = AWB;
    }

    public String getRequestFilePath() {
        return requestFilePath;
    }

    public void setRequestFilePath(String requestFilePath) {
        this.requestFilePath = requestFilePath;
    }

    public String getEndpointPath() {
        return endpointPath;
    }

    public void setEndpointPath(String endpointPath) {
        this.endpointPath = endpointPath;
    }

    public String getExpectedContentType() {
        return expectedContentType;
    }

    public void setExpectedContentType(String expectedContentType) {
        this.expectedContentType = expectedContentType;
    }

    public String getExpectedStatusCode() {
        return expectedStatusCode;
    }

    public void setExpectedStatusCode(String expectedStatusCode) {
        this.expectedStatusCode = expectedStatusCode;
    }

    public HashMap<String, String> getExpectedBQProperties() {
        return expectedBQProperties;
    }

    public void setExpectedBQProperties(HashMap<String, String> expectedBQProperties) {
        this.expectedBQProperties = expectedBQProperties;
    }

    public HashMap<String, String> getRequestHeaders() {
        return requestHeaders;
    }

    public void setRequestHeaders(HashMap<String, String> requestHeaders) {
        this.requestHeaders = requestHeaders;
    }

    public HashMap<String, String> getBqDataValidators() {
        return bqDataValidators;
    }

    public void setBqDataValidators(HashMap<String, String> bqDataValidators) {
        this.bqDataValidators = bqDataValidators;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public BookingRequestBuilder getBookingRequestBuilder() {
        return bookingRequestBuilder;
    }

    public void setBookingRequestBuilder(BookingRequestBuilder bookingRequestBuilder) {
        this.bookingRequestBuilder = bookingRequestBuilder;
    }
}
