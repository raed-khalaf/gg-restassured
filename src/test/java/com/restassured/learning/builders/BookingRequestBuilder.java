package com.restassured.learning.builders;

import com.restassured.learning.common.utils.OpenFreightUtils;
import org.apache.commons.lang3.StringUtils;
import org.openfreight.api.transformation.data.v1.booking.request.BookingRequest;
import org.openfreight.api.transformation.data.v1.booking.response.BookingResponse;
import org.openfreight.api.transformation.data.v1.common.BusinessInfoType;
import org.openfreight.api.transformation.data.v1.common.ConnectionType;
import org.openfreight.api.transformation.data.v1.common.DocumentType;
import org.openfreight.api.transformation.data.v1.common.IDType;
import org.openfreight.api.transformation.data.v1.common.MessageHeaderType;
import org.openfreight.api.transformation.data.v1.common.SegmentCapacityType;
import org.openfreight.api.transformation.data.v1.common.SegmentType;
import org.openfreight.api.transformation.data.v1.common.ShipmentType;
import org.openfreight.api.transformation.data.v1.common.TextType;
import org.openfreight.api.transformation.data.v1.rates.request.RateRequest;
import org.openfreight.api.transformation.data.v1.rates.response.RateResponse;
import org.openfreight.api.transformation.data.v1.rates.response.RateType;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;

/**
 *
 * Build Booking Request for (BOOK, CANCEL)
 *
 */
public class BookingRequestBuilder {

    private BookingRequest createBookingRequest;
    private BookingResponse createBookingResponse;
    private RateRequest rateRequest;
    private RateResponse rateResponse;
    private String awb;


    public BookingRequestBuilder( RateRequest rateRequest, RateResponse rateResponse, String awb) {
        this.rateRequest =  rateRequest;
        this.rateResponse = rateResponse;
        this.awb = awb;
    }

    public BookingRequestBuilder(BookingRequest createBookingRequest, BookingResponse createBookingResponse) {
        this.createBookingRequest = createBookingRequest;
        this.createBookingResponse = createBookingResponse;
    }

    public void setCreateBookingRequest(BookingRequest createBookingRequest) {
        this.createBookingRequest = createBookingRequest;
    }

    public void setCreateBookingResponse(BookingResponse createBookingResponse) {
        this.createBookingResponse = createBookingResponse;
    }

    public BookingRequest build(){


        if (createBookingRequest != null){
            createBookingRequest.getBusinessInfo().setServiceMethod(new TextType("CANCEL"));

            String hash = createBookingResponse.getBookingRate().getHash();
            if (StringUtils.isNotEmpty(hash)){
                createBookingRequest.getShipment().setHash(hash);
            }
            return createBookingRequest;
        }

        RateType rate = getBookableRate();

        ShipmentType reqShipment = rateRequest.getShipment();

        BookingRequest bookingRequest = new BookingRequest();

        setMessageHeader(bookingRequest);

        setBusinessInfo(bookingRequest);

        bookingRequest.setDocumentIdentifiers(OpenFreightUtils.buildDocumentIdentifiers(awb, DocumentType.AWB));

        setShipment(bookingRequest, rate, reqShipment);

        setCreateBookingRequest(bookingRequest);
        return bookingRequest;
    }

    private void setMessageHeader(BookingRequest bookingRequest) {
        MessageHeaderType messageHeader = new MessageHeaderType();
        messageHeader.setMessageID(new IDType(""));
        messageHeader.setRequestID(new IDType(""));

        bookingRequest.setMessageHeader(messageHeader);
    }

    private void setBusinessInfo(BookingRequest bookingRequest) {
        BusinessInfoType businessInfoType = new BusinessInfoType();
        businessInfoType.setStation(rateRequest.getBusinessInfo().getStation());
        businessInfoType.setServiceMethod(new TextType("BOOK"));
        businessInfoType.setMethodReason("Any");
        businessInfoType.setParties(rateRequest.getBusinessInfo().getParties());
        bookingRequest.setBusinessInfo(businessInfoType);
    }

    private void setShipment(BookingRequest bookingRequest, RateType rate, ShipmentType reqShipment) {
        ShipmentType shipment = new ShipmentType();

        //Items that will be set from the returned route
        shipment.setOriginLocation(rate.getOriginLocation());
        shipment.setDestinationLocation(rate.getDestinationLocation());

        setRequestDates(rate, reqShipment, shipment);


        setConnection(rate, shipment);

        if (StringUtils.isNotEmpty(rate.getHash())){
            shipment.setHash(rate.getHash());
        }

        //items that will be set from the original availabilityRequest
        shipment.setShipmentPriorityCode(reqShipment.getShipmentPriorityCode());
        shipment.setLoad(reqShipment.getLoad());
        shipment.setPricePreference(reqShipment.getPricePreference());
        //Commodity is taken from the original availability request since not all airlines return the code and the name of the commodity
        shipment.setCommodityClassification(reqShipment.getCommodityClassification());

        bookingRequest.setShipment(shipment);
    }

    private void setRequestDates(RateType rate, ShipmentType reqShipment, ShipmentType shipment) {
        XMLGregorianCalendar timeOfAvailability = rate.getTimeOfAvailability();
        if (timeOfAvailability == null) {
            timeOfAvailability = reqShipment.getTimeOfAvailability();
        }
        XMLGregorianCalendar latestAcceptanceDate = rate.getLatestAcceptanceDate();
        if (latestAcceptanceDate == null) {
            latestAcceptanceDate = reqShipment.getLatestAcceptanceDate();
        }
        shipment.setTimeOfAvailability(timeOfAvailability);
        shipment.setLatestAcceptanceDate(latestAcceptanceDate);
    }

    private void setConnection(RateType rate, ShipmentType shipment) {
        ConnectionType connectionType = new ConnectionType();
        connectionType.setConnectionSegments(new ConnectionType.ConnectionSegments());
        connectionType.getConnectionSegments().setSegment(new ArrayList<SegmentType>());

        ConnectionType.ConnectionSegments connectionSegments = new ConnectionType.ConnectionSegments();
        SegmentType segmentType;
        for (SegmentCapacityType seg : rate.getConnection().getConnectionSegments().getSegment()) {
            segmentType = new SegmentType();
            segmentType.setOrigin(seg.getOrigin());
            segmentType.setDestination(seg.getDestination());
            segmentType.setItineraryNumber(seg.getItineraryNumber());
            segmentType.setScheduledTimeOfArrival(seg.getScheduledTimeOfArrival());
            segmentType.setScheduledTimeOfDeparture(seg.getScheduledTimeOfDeparture());
            segmentType.setOperatingCarrier(seg.getOperatingCarrier());
            connectionType.getConnectionSegments().getSegment().add(segmentType);
        }
        shipment.setConnection(connectionType);
    }

    private RateType getBookableRate(){

        RateType bookableRate = null;
        for (RateType rateType : rateResponse.getRates().getRate()) {
            if (rateType.getRateStatus() == null || rateType.getRateStatus().getCode().getValue().equals("BOOKABLE")) {
                bookableRate = rateType;
                break;
            }
        }

        if (bookableRate == null) {

            //todo: handle this case. No bookable offer is returned
        }
        return bookableRate;
    }
}
